import './App.css';
import Numbers from "./Numbers/Numbers";
import {Component} from "react";

class App extends Component {
    state ={
        randomNumbers: [1, 2, 5, 4, 5],
        array: []
    }
    getRandomNumber = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1) + min)
    }

    fillNewRandomNumbers =() => {
        const newArray = [];
        while (newArray.length < 5) {
            const num = this.getRandomNumber(5, 36);
            if (!newArray.includes(num)) {
                newArray.push(num);
            }
        }
        this.setState({ randomNumbers: newArray.sort((a, b) => a > b ? 1 : -1)});
    }

    changeNumbers = ()=>{
        this.fillNewRandomNumbers();
    }


    render() {
        return (
            <div className="App">
                <Numbers randomNumbers = {this.state.randomNumbers[0]}/>
                <Numbers randomNumbers = {this.state.randomNumbers[1]}/>
                <Numbers randomNumbers = {this.state.randomNumbers[2]}/>
                <Numbers randomNumbers = {this.state.randomNumbers[3]}/>
                <Numbers randomNumbers = {this.state.randomNumbers[4]}/>
                <button onClick={this.changeNumbers}>New numbers</button>
            </div>
        );
    }
}

export default App;
